package com.sandbox.laba4

/**
 * Сатический класс билдер для матрицы.
 * Имеет метод для создания матрицы.
 * @param mtrxSize - размер матрицы любое значение Int > 0.
 * @return Возвращает пустую матрицу SquareMatrix размера mtrxSize.
 */

object BuilderMatrix {
    fun build(mtrxSize: Int): SquareMatrix {
        if (mtrxSize <= 0) {
            throw IllegalArgumentException()
        }
        return SquareMatrix(mtrxSize)
    }
}
