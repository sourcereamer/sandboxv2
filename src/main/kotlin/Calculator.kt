package com.sandbox

/**
* @param a the type of a Integer.
* @param b the type of a Integer.
* @return Integer (sum of a and b).
 * */
class Calculator {
    fun add(a: Int, b: Int): Int {
        return a + b
    }
}
