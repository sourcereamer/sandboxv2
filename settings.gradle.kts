rootProject.name = "sandbox"

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
    }
}
